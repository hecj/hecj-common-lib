package com.hecj.common.lib.excel;

public class Column {
	// 数据库字段名
	private String field;
	// 显示表头
	private String title;

	public Column() {
		super();
	}

	public Column(String field, String title) {
		super();
		this.field = field;
		this.title = title;
	}

	public String getField() {
		return field;
	}

	public void setField(String field) {
		this.field = field;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}
}