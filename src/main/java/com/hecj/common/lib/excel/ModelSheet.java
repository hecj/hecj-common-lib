package com.hecj.common.lib.excel;

import java.util.List;

/**
 * @类功能说明：支持从jfinal model中根据字段名导出
 * @类修改者：
 * @修改日期：
 * @修改说明：
 * @作者：He Chaojie
 * @创建时间：2017-6-5 上午10:50:43
 * @版本：V1.0
 */
public class ModelSheet extends AbstractSheet {

	private static final long serialVersionUID = 1L;
	protected List<Column> columns;
	protected List<?> datas;
	protected String sheetName;
	protected String excelName;

	public ModelSheet() {
		super();
	}

	/**
	 * @类名：SimpleExcel.java
	 * @描述：列名，数据集合
	 * @param colNames
	 * @param datas
	 */
	public ModelSheet(List<Column> columns, List<?> datas) {
		super();
		this.columns = columns;
		this.datas = datas;
	}

	public List<Column> getColumns() {
		return columns;
	}

	public void setColumns(List<Column> columns) {
		this.columns = columns;
	}

	public List<?> getDatas() {
		return datas;
	}

	public void setDatas(List<?> datas) {
		this.datas = datas;
	}

	public String getSheetName() {
		return sheetName;
	}

	public void setSheetName(String sheetName) {
		this.sheetName = sheetName;
	}

	public String getExcelName() {
		return excelName;
	}

	public void setExcelName(String excelName) {
		this.excelName = excelName;
	}
	
}
