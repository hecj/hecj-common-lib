package com.hecj.common.lib.exception;

/**
 * 文件权限异常
 */
public class FilePermissionsException extends Exception {

	private static final long serialVersionUID = -658996053166386425L;
	
	public FilePermissionsException(String message) {
		super(message);
	}

}
