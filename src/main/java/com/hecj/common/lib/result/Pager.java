package com.hecj.common.lib.result;

import java.io.Serializable;

/**
 * @类功能说明：简单封装分页器
 * @类修改者：
 * @修改日期：
 * @修改说明：
 * @作者：HECJ
 * @创建时间：2014-12-5 上午11:34:07
 * @版本：V1.0
 */
public class Pager implements Serializable, Cloneable {

	private static final long serialVersionUID = 1L;
	
	/*
	 * 当前页码
	 */
	private Integer pageNumber = 1;
	/*
	 * 每页条数
	 */
	private Integer pageSize = 20;
	/*
	 * 总条数
	 */
	private Long countSize = 0l;

	public Pager() {
		super();
	}
	public Pager(Integer pageSize) {
		this.pageSize = pageSize;
	}

	public Long getCountPage() {
		if(this.getCountSize()%this.getPageSize() == 0){
			return this.getCountSize()/this.getPageSize();
		}else{
			return this.getCountSize()/this.getPageSize() + 1;
		}
	}

	public Integer getPageSize() {
		return pageSize;
	}

	public void setPageSize(Integer pageSize) {
		this.pageSize = pageSize;
	}
	public void setPageSize(Integer pageSize,Integer defaultValue) {
		if(pageSize == null){
			this.pageSize = defaultValue;
		} else{
			this.pageSize = pageSize;
		}
		
	}

	public Integer getPageNumber() {
		return pageNumber;
	}
	public void setPageNumber(Integer pageNumber,Integer defaultValue) {
		if(pageNumber == null){
			this.pageNumber = defaultValue;
		} else{
			this.pageNumber = pageNumber;
		}
	}
	public void setPageNumber(Integer pageNumber) {
		this.pageNumber = pageNumber;
	}
	public Long getCountSize() {
		return countSize;
	}

	public void setCountSize(Long countSize) {
		this.countSize = countSize;
	}

	public Integer getStartCursor() {
		return (this.pageNumber - 1) * this.pageSize;
	}
	
}