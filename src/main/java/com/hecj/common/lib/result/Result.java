package com.hecj.common.lib.result;
import java.io.Serializable;
import java.util.Map;

/**
 * @类功能说明：数据集
 * @类修改者：
 * @修改日期：
 * @修改说明：
 * @作者：HECJ
 * @创建时间：2014-12-21 上午11:30:08
 * @版本：V1.0
 */
public class Result implements Serializable {
    
    private static final long serialVersionUID = -2040819162899511964L;
    
    protected Long code = 200l;
    
    protected Object data;
    
    protected String message = "success";
    // 扩展字段 map格式
    protected Map<String,Object> ext;
    
    public Result() {
    
    }
    
    public Result(Long code) {
        this.code = code;
    }
    
    public Result(Long code, String message) {
        this.code = code;
        this.message = message;
    }
    
    public Result(Long code, Object data) {
        this.code = code;
        this.data = data;
    }
    
    public Result(Long code, String message, Object data) {
        this.code = code;
        this.data = data;
        this.message = message;
    }
    
    public Result(CODE code) {
        this.code = code.value;
        this.message = code.message;
    }
    
    public Result(CODE code, String message) {
        this.code = code.value;
        this.message = message;
    }
    
    public Result(CODE code, Object data) {
        this.code = code.value;
        this.message = code.message;
        this.data = data;
    }
    
    public Result(CODE code, String message, Object data) {
        this.code = code.value;
        this.message = message;
        this.data = data;
    }
    
    public Long getCode() {
        return code;
    }
    
    
    public Result setCode(Long code) {
        this.code = code;
        return this;
    }
    
    
    public Object getData() {
        return data;
    }
    
    
    public Result setData(Object data) {
        this.data = data;
        return this;
    }
    
    
    public String getMessage() {
        return message;
    }
    
    
    public Result setMessage(String message) {
        this.message = message;
        return this;
    }
    
    public Map<String,Object> getExt() {
        return ext;
    }
    
    public void setExt(Map<String,Object> ext) {
        this.ext = ext;
    }
    
    public enum CODE{
        SUCCESS(200l,"success"),
        SIGN_ERROR(-1l,"接口签名错误"),
        LOGIN_INVALID(-999l,"登录失效"),
        EXCEPTION(-100000,"处理错误,请稍后再试")
        ;
        long value;
        String message;
        CODE(long value,String message){
            this.value = value;
            this.message = message;
        }
    }
    
}