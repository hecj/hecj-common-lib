package com.hecj.common.lib.result;

import java.util.HashMap;
import java.util.Map;

/**
 * @类功能说明：基本实现类 默认map的key为data保存基本数据集合 扩展数据可放入map中 @类修改者： @修改日期： @修改说明：
 * @作者：HECJ @创建时间：2014-12-21 上午11:23:04 @版本：V1.0
 */
public class ResultData extends Result {

	private static final long serialVersionUID = 5576703601426266833L;

	private Map<String, Object> data = new HashMap<String, Object>();

	public Map<String, Object> getData() {
		return data;
	}

	public Result setData(Map<String, Object> data) {
		this.data = data;
		return this;
	}
	
	public Result put(String key,Object value) {
		this.data.put(key, value);
		return this;
	}
}