package com.hecj.common.lib.result;

import java.util.ArrayList;
import java.util.List;

import com.github.pagehelper.Page;

/**
 * @类功能说明：基本实现类 @类修改者： @修改日期： @修改说明：
 * @作者：HECJ @创建时间：2014-12-21 上午11:23:04 @版本：V1.0
 */
public class ResultPager extends Result {

	private static final long serialVersionUID = 5576703601426266833L;

	private List<?> list;

	private Pager pager ;
	
	public ResultPager(long code) {
		this.code = code;
	}

	public ResultPager(long code,List<?> list) {
		this.code = code;
		this.list = list;
	}

	public Result setList(List<?> list) {
		this.list = list;
		return this;
	}

	public List<?> getList() {
		return this.list;
	}

	public Pager getPager() {
		return this.pager;
	}

	public Result setPager(Pager pager) {
		this.pager = pager;
		return this;
	}

	/**
	 * 分页插件转换
	 */
	public ResultPager(long code,Page<?> page) {
		this.list = page.getResult();
		Pager pager = new Pager();
		pager.setPageSize(page.getPageSize());
		pager.setCountSize(page.getTotal());
		pager.setPageNumber(page.getPageNum());
		this.setPager(pager);
		this.code = code;
	}

	public Result setPageData(Page<?> page) {
		List<?> mList = page.getResult();
		List<Object> dataList = new ArrayList<Object>();
		for (Object obj : mList) {
			dataList.add(obj);
		}
		this.list = dataList;
		Pager pager = new Pager();
		pager.setPageSize(page.getPageSize());
		pager.setCountSize(page.getTotal());
		pager.setPageNumber(page.getPageNum());
		this.setPager(pager);
		return this;
	}
}