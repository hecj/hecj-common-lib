package com.hecj.common.lib.util;

import java.util.Properties;

import com.alibaba.druid.util.DruidPasswordCallback;
import com.alibaba.druid.util.StringUtils;

import com.hecj.common.lib.encryption.Encrypt;

public class DBPasswordCallback extends DruidPasswordCallback {

	private static final long serialVersionUID = -5720700454940285353L;

	@Override
	public void setProperties(Properties properties) {
		super.setProperties(properties);
		String password = properties.getProperty("password");  
        if (!StringUtils.isEmpty(password)) {  
            try {  
                String decodePassword = Encrypt.strDecode(password, Constant.DesKey, "", "");
                setPassword(decodePassword.toCharArray());  
            } catch (Exception e) {  
                setPassword(password.toCharArray());  
            }  
        }  
	}
	
}
