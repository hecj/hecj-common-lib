package com.hecj.common.lib.util;

import java.util.*;

/**
 * 描述：Id生成规则
 * @author: hecj
 */
public class GenerateUtil {
	
	final static char[] RANDOM_CHAR = {'a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k',
			'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w',
			'x', 'y', 'z', 'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K',
			'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W',
			'X', 'Y', 'Z', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9' ,'_','='};

	/**
	 * 描述：根据时间戳生成Id
	 * @author: hecj
	 */
	public static String generateId(){
		
		return generateId(3);
	}
	
	public static String getUUID(){
		return UUID.randomUUID().toString().replace("-", "");
	}
	
	public static String generateId(int num){
		
		return generateId(new Date(),num);
	}
	
	public static String generateId(Date date,int num){
		return DateFormatUtil.format(date, "yyyyMMddHHmmssSSS")+randomNumber(num);
	}
	
	public static String randomNumber(int num){
		StringBuffer str = new StringBuffer();
		Random ran = new Random();
		for (int i = 0; i < num; i++) {
			str.append(String.valueOf(ran.nextInt(10)));
		}
		return str.toString();
	}
	
	/**
	 * 生成随机字符串
	 * @return
	 */
	public static String genRandomChar(int num){
		int maxNum = RANDOM_CHAR.length;
		int i;
		int count = 0;
		StringBuffer chars = new StringBuffer();
		Random r = new Random();
		while(count < num){
			i = Math.abs(r.nextInt(maxNum));
			if (i >= 0 && i < maxNum) {
				chars.append(RANDOM_CHAR[i]);
				count ++;
			}
		}
		return chars.toString();
	}
	
}
