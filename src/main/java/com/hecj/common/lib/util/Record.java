package com.hecj.common.lib.util;

import java.util.HashMap;
/**
 * 数据封装
 * @author hecj
 */
public class Record extends HashMap<String, Object> {

	private static final long serialVersionUID = 1L;
	
	public Integer getInt(String key){
		return (Integer)(get(key));
	}
	
	public Long getLong(String key){
		return (Long)(get(key));
	}
	
	public String getStr(String key){
		return (String)(get(key));
	}
	
	@Override
	public Record put(String key,Object value){
		super.put(key, value);
		return this;
	}
}




