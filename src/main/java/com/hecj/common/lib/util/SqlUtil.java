package com.hecj.common.lib.util;

import java.util.List;

public class SqlUtil {
	
	/**
	 * 逗号分隔
	 * @author hecj
	 * @param list
	 * @return
	 */
	public static String splitStr(List<String> list){
		StringBuffer ids = new StringBuffer();
		list.forEach(id ->{
			ids.append(","+"'"+id+"'");
		});
		return ids.toString().replaceFirst(",","");
	}
	
	/**
	 * 逗号分隔
	 * @author hecj
	 * @param list
	 * @return
	 */
	public static String splitLong(List<Long> list){
		StringBuffer ids = new StringBuffer();
		list.forEach(id ->{
			ids.append(","+id);
		});
		return ids.toString().replaceFirst(",","");
	}
}
