package com.hecj.common.lib.util;

import java.util.HashSet;
import java.util.UUID;

public class UUIDGenerator {

	public UUIDGenerator() {  
    }  
  
	//生成一组UUID
    public static String getUUID() {  
    	return UUID.randomUUID().toString().replace("-", "");
    }  
    
    //获得指定数量且不重复的UUID Set集合 
    public static HashSet<String> getUUID(int number) {  
        if (number < 1) {  
            return null;  
        }  
        HashSet<String> set = new HashSet<String>();
        int i=0;
        for(;i<number;i++){
        	set.add(getUUID());
        }
    	return set;
    } 
}
